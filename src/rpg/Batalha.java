package rpg;

import java.util.Random;
import personagens.Capitao;
import personagens.Cavaleiro;
import personagens.Curandeira;
import personagens.Solerte;

/**
 * Batalha é a classe onde acontece o sorteio dos personagens que vão se enfrentar e sua batalha efetiva. 
 * Sorteia um personagem do exército 1 e batalha todos eles com os sorteados do exército 2(bot).
 * Utiliza seu ataque para definir o ganhador do enfrentamento.
 * Quando um personagem perde ele é excluído da lista.
 * Utiliza os personagens vivos no final para definir o ganhador através de seu ataque e defesa.
 * 
 * @author larac e kamillek
 */

public class Batalha {
    Random random = new Random();
    //Objetos de acesso aos métodos dos personagens
    static Cavaleiro cav = new Cavaleiro();
    static Capitao cap = new Capitao();
    static Curandeira cur = new Curandeira();
    static Solerte sol = new Solerte();
    
    //Personagem sorteado e mediador para verificação se o personagem já foi sorteado anteriormente
    int personagem, mediador;
    //Armazena o tamanho dos personagens do ex1
    int tcap = rpg.JogarController.e.tamCapitao(), tcav = rpg.JogarController.e.tamCavaleiro(), tcur = rpg.JogarController.e.tamCurandeira(), tsol = rpg.JogarController.e.tamSolerte();
    //Vetor para armazenar os personagens já sorteados do ex1
    int[] personOk = new int[4];
    //Exército vencedor
    static int venc;
    //Tamanho dos exércitos
    static int tamEx1, tamEx2;
    //Defesa e ataque dos exércitos
    int def1, def2;
    int atk1, atk2;
    
    /**
     * Inicializa as 4 posições de um vetor em -1 para armazenar os personagens do exéricito 1 sorteados anteriormente.
     */
    public void inicializa() {
        personOk[0] = -1;
        personOk[1] = -1;
        personOk[2] = -1;
        personOk[3] = -1;
    }

    /**
     * Sorteia um personagem do exército 1 (0,1,2,3) e verifica se o mesmo já não foi sorteado anteriormente analisando as posições do vetor personOk[].
     * Caso não tenha sido, promove a batalha entre todos os membros da lista desse personagem com personagens sorteados so ex 2.
     */
    public void batalhar() {
        inicializa();
        //Saída de dados para acompanhar a execussão do programa
        System.out.println("Ataque: \nCav: "+cav.getAtaque());
        System.out.println("Cap: "+cap.getAtaque());
        System.out.println("Cur: "+cur.getAtaque());
        System.out.println("Sol: "+sol.getAtaque());
        System.out.println("Defesa:\nCav: "+cav.getDefesa());
        System.out.println("Cap: "+cap.getDefesa());
        System.out.println("Cur: "+cur.getDefesa());
        System.out.println("Sol: "+sol.getDefesa());
        
        System.out.println("=====================================\nSorteia Ex1: ");
        for (int cont = 0; cont < 4; cont++) {
            //mediador recebe o número sorteado
            mediador = sorteiaPersonagem();
            //verifica se o personagem já voi sorteado antes analisando um veror de 4 posições que contém as posições sorteadas
            for (int cont2 = 0; cont2 < 4; cont2++) {
                while (mediador == personOk[cont2]) {
                    mediador = sorteiaPersonagem();
                    cont2 = 0;
                }
            }
            //caso não tenha sido sorteado anteriormente, atribui o personagem sorteado ao vetor
            personOk[cont] = mediador;
            //chama a função correspondente ao personagem sorteado do 1º exército
            switch (personOk[cont]) {
                case 0: 
                    //Cavaleiro ex1
                    System.out.print("Cavaleiro\n");
                    batalharCavaleiro();
                    break;
                case 1: 
                    //Capitão ex1
                    System.out.print("Capitao\n");
                    batalharCapitao();
                    break;
                case 2: 
                    //Curandeira ex1
                    System.out.print("Curandeira\n");
                    batalharCurandeira();
                    break;
                case 3: 
                    //Solerte ex1
                    System.out.print("Solerte\n");
                    batalharSolerte();
                    break;
            }
        }
        //chama a função que analisa dados da batalha e dos personagens para definir o ganhador
        definirGanhador();
    }

    /**
     * É chamado no método Batalhar quando o personagem sorteado no exército 1 é um cavaleiro.
     * Faz a batalha entre todos os membros da lista de cavaleiros do ex1 com personagens sorteados do exército 2.
     * Após batalhar todos os personagens e excluir das listas os perdedores, retorna para o método Batalha, onde acontece o sorteiode um novo personagem do exército 1.
     */
    public void batalharCavaleiro() {
        System.out.println("\nSorteia Ex2: ");
        for (int cont = 0; cont < tcav; cont++) {
            //CAVALEIRO EXÉRCITO 1
            switch (sorteiaPersonagem()) {

                case 0: //x Cavaleiro ex2
                    //persosnagens iguais, ataque igual
                    System.out.println("= cavaleiro");
                    System.out.println("Vencedor: empate");
                    break;

                case 1: //x Capitão ex2
                    System.out.println("= capitao");
                    if (cav.getAtaque() > cap.getAtaque()) {
                        if(rpg.JogarController.e2.tamCapitao()>0){
                            rpg.JogarController.e2.removerCapitao();
                            System.out.println("Vencedor: cavaleiro ex1");
                        }
                    } else {
                        if(rpg.JogarController.e.tamCapitao()>0){
                            rpg.JogarController.e.removerCavaleiro();
                            System.out.println("Vencedor: capitao ex2");
                        }
                    }
                    break;

                case 2: //x Curandeira ex2
                    System.out.println("= curandeira");
                    if (cav.getAtaque() > cur.getAtaque()) {
                        if(rpg.JogarController.e2.tamCurandeira()>0){
                            rpg.JogarController.e2.removerCurandeira();
                            System.out.println("Vencedor: cavaleiro ex1");
                        }
                    } else {
                        if(rpg.JogarController.e.tamCavaleiro()>0){
                            rpg.JogarController.e.removerCavaleiro();
                            System.out.println("Vencedor: curandeira ex2");
                        }
                    }
                    break;

                case 3: //x Solerte ex2
                    System.out.println("= solerte");
                    cav.ataqueEspecial(cav.getAtaque());
                    System.out.println("ATAQUE ESPECIAL CAVALEIRO: " + cav.getAtaque());
                    if (cav.getAtaque() > sol.getAtaque()) {
                        if(rpg.JogarController.e2.tamSolerte()>0){
                            rpg.JogarController.e2.removerSolerte();
                            System.out.println("Vencedor: cavaleiro ex1");
                        }
                    } else {
                        if(rpg.JogarController.e.tamCavaleiro()>0){
                            rpg.JogarController.e.removerCavaleiro();
                            System.out.println("Vencedor: solerte ex2");
                        }
                    }
                    break;
            }
        }
        System.out.println("\n***Cavaleiros restantes no Ex1: " + rpg.JogarController.e.tamCavaleiro() + "\n=====================================");
    }
    
    /**
     * É chamado no método Batalhar quando o personagem sorteado no exército 1 é um capitão.
     * Faz a batalha entre todos os membros da lista de capitões do ex1 com personagens sorteados do exército 2.
     * Após batalhar todos os personagens e excluir das listas os perdedores, retorna para o método Batalha, onde acontece o sorteiode um novo personagem do exército 1.
     */
    public void batalharCapitao() {
        System.out.println("\nSorteia Ex2: ");
        for (int cont = 0; cont < tcap; cont++) {
            //CAPITÃO EXÉRCITO 1
            switch (sorteiaPersonagem()) {

                case 0: //x Cavaleiro ex2
                    System.out.println("= cavaleiro");
                    if (cap.getAtaque() > cav.getAtaque()) {
                        if(rpg.JogarController.e2.tamCapitao()>0){
                            rpg.JogarController.e2.removerCavaleiro();
                            System.out.println("Vencedor: capitao ex1");
                        }
                    } else {
                        if(rpg.JogarController.e.tamCapitao()>0){
                            rpg.JogarController.e.removerCapitao();
                            System.out.println("Vencedor: cavaleiro ex2");
                        }
                    }
                    break;

                case 1: //x Capitão ex2 
                    System.out.println("= capitão\nVencedor: empate");
                    break;

                case 2: //x Curandeira ex2
                    System.out.println("= curandeira");
                    cap.ataqueEspecial(cap.getAtaque());
                    System.out.println("ATAQUE ESPECIAL CAPITÃo: "+ cap.getAtaque());
                    if (cap.getAtaque() > cur.getAtaque()) {
                        if(rpg.JogarController.e2.tamCurandeira()>0){
                            rpg.JogarController.e2.removerCurandeira();
                            System.out.println("Vencedor: capitao ex1");
                        }
                    } else {
                        if(rpg.JogarController.e.tamCapitao()>0){
                            rpg.JogarController.e.removerCapitao();
                            System.out.println("Vencedor: curandeira ex2");
                        }
                    }
                    break;

                case 3: //x Solerte ex2
                    System.out.println("= solerte");
                    if (cap.getAtaque() > sol.getAtaque()) {
                        if(rpg.JogarController.e2.tamSolerte()>0){
                            rpg.JogarController.e2.removerSolerte();
                            System.out.println("Vencedor: capitao ex1");
                        }
                    } else {
                        if(rpg.JogarController.e.tamCapitao()>0){
                            rpg.JogarController.e.removerCapitao();
                        System.out.println("Vencedor: solerte ex2");}
                    }
                    break;
            }
        }
        System.out.println("\n***Capitães restantes no Ex1: " + rpg.JogarController.e.tamCapitao() + "\n=====================================");
    }
    
    /**
     * É chamado no método Batalhar quando o personagem sorteado no exército 1 é uma curandeira.
     * Faz a batalha entre todos os membros da lista de curandeiras do ex1 com personagens sorteados do exército 2.
     * Após batalhar todos os personagens e excluir das listas os perdedores, retorna para o método Batalha, onde acontece o sorteiode um novo personagem do exército 1.
     */
    public void batalharCurandeira() {
        System.out.println("\nSorteia Ex2: ");
        for (int cont = 0; cont < tcur; cont++) {
            //CURANDEIRA EXÉRCITO 1
            switch (sorteiaPersonagem()) {

                case 0: //x Cavaleiro ex2
                    System.out.println("= cavaleiro");
                    if (cur.getAtaque() > cav.getAtaque()) {
                        if(rpg.JogarController.e.tamCavaleiro()>0){
                            rpg.JogarController.e2.removerCavaleiro();
                            System.out.println("Vencedor: Curandeira ex1");
                        }
                    } else {
                        if(rpg.JogarController.e2.tamCurandeira()>0){
                            rpg.JogarController.e.removerCurandeira();
                            System.out.println("Vencedor: cavaleiro ex2");
                        }
                    }
                    break;

                case 1: //x Capitão ex2
                    System.out.println("= capitão");
                    if (cur.getAtaque() > cap.getAtaque()) {
                        if(rpg.JogarController.e2.tamCapitao()>0){
                            rpg.JogarController.e2.removerCapitao();
                            System.out.println("Vencedor: Curandeira ex1");
                        }
                    } else {
                        if(rpg.JogarController.e.tamCurandeira()>0){
                            rpg.JogarController.e.removerCurandeira();
                            System.out.println("Vencedor: capitao ex2");
                        }
                    }
                    break;

                case 2: //x Curandeira ex2
                    System.out.println("= Curandeira\nVencedor: empate");
                    break;

                case 3: //x Solerte ex2
                    System.out.println("= solerte");
                    cur.ataqueEspecial(cur.getDefesa());
                    System.out.println("ATAQUE ESPECIAL CURANDEIRA - DEFESA: " + cur.getDefesa());
                    if (cur.getAtaque() > sol.getAtaque()) {
                        if(rpg.JogarController.e2.tamSolerte()>0){
                            rpg.JogarController.e2.removerSolerte();
                            System.out.println("Vencedor: Curandeira ex1");
                        }
                    } else {
                        if(rpg.JogarController.e.tamCurandeira()>0){
                            rpg.JogarController.e.removerCurandeira();
                            System.out.println("Vencedor: solerte ex2");
                        }
                    }
                    break;
            }
        }
        System.out.println("\n***Curandeiras restantes no Ex1: " + rpg.JogarController.e.tamCurandeira() + "\n=====================================");
    }
    
    /**
     * É chamado no método Batalhar quando o personagem sorteado no exército 1 é um solerte.
     * Faz a batalha entre todos os membros da lista de solertes do ex1 com personagens sorteados do exército 2.
     * Após batalhar todos os personagens e excluir das listas os perdedores, retorna para o método Batalha, onde acontece o sorteiode um novo personagem do exército 1.
     */
    public void batalharSolerte() {
        System.out.println("\nSorteia Ex2: ");
        for (int cont = 0; cont < tsol; cont++) {
            //SOLERTE EXÉRCITO 1
            switch (sorteiaPersonagem()) {

                case 0: //x Cavaleiro ex2
                    System.out.println("= cavaleiro");
                    if (sol.getAtaque() > cav.getAtaque()) {
                        if(rpg.JogarController.e2.tamCavaleiro()>0){
                            rpg.JogarController.e2.removerCavaleiro();
                            System.out.println("Vencedor: solerte ex1");
                        }
                    } else {
                        if(rpg.JogarController.e2.tamSolerte()>0){
                            rpg.JogarController.e.removerSolerte();
                            System.out.println("Vencedor: cavaleiro ex2");
                        }
                    }
                    break;

                case 1: //x Capitão ex2
                    System.out.println("= capitão");
                    if (sol.getAtaque() > cap.getAtaque()) {
                        if(rpg.JogarController.e2.tamCapitao()>0){
                            rpg.JogarController.e2.removerCapitao();
                            System.out.println("Vencedor: solerte ex1");
                        }
                    } else {
                        if(rpg.JogarController.e2.tamSolerte()>0){
                            rpg.JogarController.e.removerSolerte();
                            System.out.println("Vencedor: capitao ex2");
                        }
                    }
                    break;

                case 2: //x Curandeira ex2
                    System.out.println("= curandeira");
                    sol.ataqueEspecial(sol.getAtaque());
                    System.out.println("ATAQUE ESPECIAL SOLERTE: " + sol.getAtaque());
                    if (sol.getAtaque() > cur.getAtaque()) {
                        if(rpg.JogarController.e2.tamCurandeira()>0){
                            rpg.JogarController.e2.removerCurandeira();
                            System.out.println("Vencedor: solerte ex1");
                        }
                    } else {
                        if(rpg.JogarController.e.tamSolerte()>0){
                            rpg.JogarController.e.removerSolerte();
                            System.out.println("Vencedor: curandeira ex2");
                        }
                    }
                    break;

                case 3: //x Solerte ex2
                    //personagens iguais, ataque igual
                    System.out.println("= solerte\nVencedor: empate");
                    break;
            }
        }
        System.out.println("\n***Solertes restantes no Ex1: " + rpg.JogarController.e.tamSolerte() + "\n=====================================");
    }

    /**
     * O método definirGanhador calcula o ganhador com base no ataque e defesa dos exércitos.
     * Atribui à variável venc o exército vencedor.
     */
    public void definirGanhador() {
        //Tamanho dos exércitos
        tamEx1 = rpg.JogarController.e.tamCapitao() + rpg.JogarController.e.tamCavaleiro() + rpg.JogarController.e.tamCurandeira() + rpg.JogarController.e.tamSolerte();
        System.out.println("Tamanho ex1: " + tamEx1);
        tamEx2 = rpg.JogarController.e2.tamCapitao() + rpg.JogarController.e2.tamCavaleiro() + rpg.JogarController.e2.tamCurandeira() + rpg.JogarController.e2.tamSolerte();
        System.out.println("Tamanho ex2: " + tamEx2);

        //Defesa e ataque do exército 1
        def1 = (cap.getDefesa() * rpg.JogarController.e.tamCapitao()) + (cav.getDefesa() * rpg.JogarController.e.tamCavaleiro()) + (cur.getDefesa() * rpg.JogarController.e.tamCurandeira()) + (sol.getDefesa() * rpg.JogarController.e.tamSolerte());
        atk1 = (cap.getAtaque() * rpg.JogarController.e.tamCapitao()) + (cav.getAtaque() * rpg.JogarController.e.tamCavaleiro()) + (cur.getAtaque() * rpg.JogarController.e.tamCurandeira()) + (sol.getAtaque() * rpg.JogarController.e.tamSolerte());
        System.out.println("Defesa ex1: " + def1);
        System.out.println("Ataque ex1: " + atk1);
        
        //Defesa e ataque do exército 2
        def2 = (cap.getDefesa() * rpg.JogarController.e2.tamCapitao()) + (cav.getDefesa() * rpg.JogarController.e2.tamCavaleiro()) + (cur.getDefesa() * rpg.JogarController.e2.tamCurandeira()) + (sol.getDefesa() * rpg.JogarController.e2.tamSolerte());
        atk2 = (cap.getAtaque() * rpg.JogarController.e2.tamCapitao()) + (cav.getAtaque() * rpg.JogarController.e2.tamCavaleiro()) + (cur.getAtaque() * rpg.JogarController.e2.tamCurandeira()) + (sol.getAtaque() * rpg.JogarController.e2.tamSolerte());
        System.out.println("Defesa ex2:" + def2);
        System.out.println("Ataque ex2: " + atk2);

        //Poder dos exércitos
        int poder1 = (def1 + atk1);
        System.out.println("Poder 1: " + poder1);
        int poder2 = (def2 + atk2);
        System.out.println("Poder 2: " + poder2);

        //Define o vencedor com base no exército de maior poder
        if (poder1 > poder2) {
            venc = 1;
        } else if (poder2 > poder1) {
            venc = 2;
        } else {
            venc = 3;
        }
    }

    /**
     * Sorteia um número aleatório que define o personagem.
     * @return personagem
     */
    public int sorteiaPersonagem() {
        personagem = random.nextInt(4);
        System.out.print("\nPersonagem sorteado: " + personagem + " ");
        return personagem;
    }
    
}
