package rpg;
import java.util.ArrayList;
import java.util.Random;
import personagens.Capitao;
import personagens.Cavaleiro;
import personagens.Curandeira;
import personagens.Solerte;

/**
 * Classe Exercito que possui arraylists para cada personagem e métodos para sua criação e remoção
 * @author larac e kamillek
 */
public class Exercito {
    private ArrayList <Capitao> capitao = new ArrayList <> ();
    private ArrayList <Curandeira> curandeira = new ArrayList <> ();
    private ArrayList <Cavaleiro> cavaleiro = new ArrayList <> ();
    private ArrayList <Solerte> solerte = new ArrayList <> ();

    //getters e setters
    public ArrayList<Capitao> getCapitao() {
        return capitao;
    }
    public void setCapitao(ArrayList<Capitao> capitao) {
        this.capitao = capitao;
    }
    public ArrayList<Curandeira> getCurandeira() {
        return curandeira;
    }
    public void setCurandeira(ArrayList<Curandeira> curandeira) {
        this.curandeira = curandeira;
    }
    public ArrayList<Cavaleiro> getCavaleiro() {
        return cavaleiro;
    }
    public void setCavaleiro(ArrayList<Cavaleiro> cavaleiro) {
        this.cavaleiro = cavaleiro;
    }
    public ArrayList<Solerte> getSolerte() {
        return solerte;
    }
    public void setSolerte(ArrayList<Solerte> solerte) {
        this.solerte = solerte;
    }
    public int tamCapitao(){
        return this.capitao.size();
    }
    public int tamCurandeira(){
        return this.curandeira.size();
    }
    public int tamCavaleiro(){
        return this.cavaleiro.size();
    }
    public int tamSolerte(){
        return this.solerte.size();
    }
    
    //Valores sorteados para a criação do bot
    public static int tamCav2, tamCap2, tamSol2, tamCur2;
    
    /**
     * Adiciona a quantidade informada em JogarController às listas do exército 1
     * @param quantCavaleiro
     * @param quantCapitao
     * @param quantCurandeira
     * @param quantSolerte 
     */
    public void criarExercito(int quantCavaleiro, int quantCapitao, int quantCurandeira, int quantSolerte){
        //chamadas das funções da criação de arrays 
        this.criarCavaleiro(quantCavaleiro);
        this.criarCapitao(quantCapitao);
        this.criarCurandeira(quantCurandeira);
        this.criarSolerte(quantSolerte);
        //teste no terminal da criação de unidades
        System.out.println("*******************\nExercito 1:");
        System.out.println("Cavaleiros: " + cavaleiro.size());
        System.out.println("Capitões: " + capitao.size());
        System.out.println("Curandeiras: " + curandeira.size());
        System.out.println("Solertes: " + solerte.size());
    }
    
    /**
     * Método que sorteia valores aleatórios para a quantidade de cada personagem do Ex2.
     * O personagem "Solerte" é criado de forma que complete 100 membros no ex2.
     */
    public void sorteioBot() {
        int sort=0, acm=0;
        Random random = new Random();
        System.out.println("*******************\nExercito 2: ");
        for(int cont=0; cont<3; cont++){
            do{
                sort = random.nextInt(33);//precisa ser menor que 33 para ficar minimamente proporcional
            }while(sort<10);//precisa ser maior que 10 para ficar minimamente proporcional
            acm = acm + sort;
            criarBot(cont, sort);
        }
        tamSol2 = 100 - acm;
        System.out.println("Solertes: " + tamSol2);
        criarSolerte(tamSol2);
    }
    
    /**
     * Método que cria o bot com as quantidades sorteadas no método sorteioBot()
     * @param cont
     * @param sort 
     */
    public void criarBot(int cont, int sort){
        switch (cont) {
                case 0:
                    tamCav2 = sort;
                    System.out.println("Cavaleiros: "+tamCav2);
                    criarCavaleiro(sort);
                    break;
                case 1:
                    tamCap2 = sort;
                    System.out.println("Capitões: "+tamCap2);
                    criarCapitao(sort);
                    break;
                case 2:
                    tamCur2 = sort;
                    System.out.println("Curandeiras: "+tamCur2);
                    criarCurandeira(sort);
                    break;
        }
    }
    
    /**
     * Método para a remoção de cavaleiros da lista
     */
    public void removerCavaleiro(){
        cavaleiro.remove(0);
    }
    /**
     * Método para a remoção de capitoes da lista
     */
    public void removerCapitao(){
        capitao.remove(0);
    }
    /**
     * Método para a remoção de curandeiras da lista
     */
    public void removerCurandeira(){
        curandeira.remove(0);
    }
    /**
     * Método para a remoção de solertes da lista
     */
    public void removerSolerte(){
        solerte.remove(0);
    }
    /**
     * Método para a adição de cavaleiros na lista
     */
    public void criarCavaleiro(int i){
        for(int cont=0; cont<i; cont++){
            cavaleiro.add(new Cavaleiro());
        }
    }
    /**
     * Método para a adição de capitões na lista
     */
    public void criarCapitao(int i){
        for(int cont=0; cont<i; cont++){
            capitao.add(new Capitao());
        }
    }
    /**
     * Método para a adição de curandeiras na lista
     */
    public void criarCurandeira(int i){
        for(int cont=0; cont<i; cont++){
            curandeira.add(new Curandeira());
        }
    }
    /**
     * Método para a adição de solertes na lista
     */
    public void criarSolerte(int i){
        for(int cont=0; cont<i; cont++){
            solerte.add(new Solerte());
        }
    }
}
