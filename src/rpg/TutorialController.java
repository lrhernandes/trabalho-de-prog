package rpg;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;

/**
 * Controller da tela 1 de tutorial
 * @author larac e kamillek
 */
public class TutorialController implements Initializable {
    
    @FXML
    private void proximo(ActionEvent event) {
        System.out.println("Botão próximo!");
        Rpg.trocaTela("Tutorial2.fxml");
    }
    
     @FXML
    private void voltar(ActionEvent event) {
        System.out.println("Botão voltar!");
        Rpg.trocaTela("TelaInicial.fxml");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
    }    
    
}
