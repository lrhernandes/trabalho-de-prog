package rpg;

/**
 * Classe dos personagens que permite settar ataque e defesa
 * @author larac e kamillek
 */
public class Personagem {
    //ataque e defesa de cada personagem
    private int ataque;
    private int defesa;
    
    /**
     * Ataque especial
     * @param ataque 
     */
    public void ataqueEspecial(int ataque){
        
    }

    //getters e setters
    public int getAtaque() {
        return ataque;
    }

    public void setAtaque(int ataque) {
        this.ataque = ataque;
    }

    public int getDefesa() {
        return defesa;
    }

    public void setDefesa(int defesa) {
        this.defesa = defesa;
    }
    
    /**
     * Método que configura ataque e defesa e setta os mesmos em valores padrão caso a cnfiguração não seja feita
     */
    public static void configurarPersonagens() {
        rpg.Batalha.cav.setAtaque(rpg.ConfigurarController.atcav);
        rpg.Batalha.cap.setAtaque(rpg.ConfigurarController.atcap);
        rpg.Batalha.cur.setAtaque(rpg.ConfigurarController.atcur);
        rpg.Batalha.sol.setAtaque(rpg.ConfigurarController.atsol);
        
        rpg.Batalha.cav.setDefesa(rpg.ConfigurarController.dtcav);
        rpg.Batalha.cap.setDefesa(rpg.ConfigurarController.dtcap);
        rpg.Batalha.cur.setDefesa(rpg.ConfigurarController.dtcur);
        rpg.Batalha.sol.setDefesa(rpg.ConfigurarController.dtsol);
        
        if(rpg.Batalha.cav.getAtaque()==0){rpg.Batalha.cav.setAtaque(90);}
        if(rpg.Batalha.cap.getAtaque()==0){rpg.Batalha.cap.setAtaque(80);}
        if(rpg.Batalha.cur.getAtaque()==0){rpg.Batalha.cur.setAtaque(10);}
        if(rpg.Batalha.sol.getAtaque()==0){rpg.Batalha.sol.setAtaque(65);}
        
        if(rpg.Batalha.cav.getDefesa()==0){rpg.Batalha.cav.setDefesa(100);}
        if(rpg.Batalha.cap.getDefesa()==0){rpg.Batalha.cap.setDefesa(90);}
        if(rpg.Batalha.cur.getDefesa()==0){rpg.Batalha.cur.setDefesa(90);}
        if(rpg.Batalha.sol.getDefesa()==0){rpg.Batalha.sol.setDefesa(80);}
    }
}