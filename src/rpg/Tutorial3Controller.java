package rpg;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * Controller da tela 3 de tutorial
 * @author larac e kamillek
 */
public class Tutorial3Controller implements Initializable {
    @FXML
    private void voltar(ActionEvent event) {
        System.out.println("Botão voltar!");
        Rpg.trocaTela("Tutorial2.fxml");
    }
    
    @FXML
    private void próximo(ActionEvent event) {
        System.out.println("Botão tela inicial!");
        Rpg.trocaTela("Tutorial4.fxml");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
