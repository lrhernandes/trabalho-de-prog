package rpg;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Classe Rpg main que inicia a reprodução gráfica e organização das telas.
 * 
 * @author larac e kamillek
 */
public class Rpg extends Application {

    private static Stage stage;
    
    /**
     * Método getter do Stage
     * 
     * @return stage
     */
    public static Stage getStage() {
        return stage;
    }

    /**
     * Método para trocar as telas da aplicação.
     * @param tela 
     */
    public static void trocaTela(String tela) {
        Parent root = null;
        try {
            root = FXMLLoader.load(Rpg.class.getResource(tela));
        } catch (IOException e) {
            System.out.println("Verificar arquivo FXML");
        }

        Scene scene = new Scene(root, 836, 487);
        stage.setScene(scene);
        stage.show();
    }
    
    /**
     * Método que inicia e determina a tela inicial do jogo
     * @param stage
     * @throws Exception 
     */
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("TelaInicial.fxml"));
        Scene scene = new Scene(root);
        Rpg.stage = stage;
        stage.setScene(scene);
        stage.show();
    }
    
    /**
     * Main da classe. Inicializa a reprodução das telas.
     * @param args 
     */
    public static void main(String[] args) {
        launch(args);
    }

}
