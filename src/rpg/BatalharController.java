package rpg;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 * Controller da tela de batalha que exibe o ganhador.
 * @author larac e kamillek
 */
public class BatalharController implements Initializable {
    //Objeto do tipo batalha
    Batalha b = new Batalha();

    @FXML
    Label batalhandoGif;

    /**
     * Método que exibe mensagem do resultado da batalha de acordo com o vencedor
     */
    @FXML
    private void exibirVencedor() {
        //Exibe mensagem de acordo com o conteúdp da variável venc na classe Batalha
        switch (rpg.Batalha.venc) {
            case 1: 
                //vencedor 1
                System.out.println("\nVENCEDOR: EXÉRCITO 1");
                batalhandoGif.setText("Você ganhou! Parabéns! seu exército derrotou os inimigos e se tornou campeão! Continue aprimorando suas estratégias de batalha, estás indo muito bem!");
                break;
            case 2: 
                //vencedor 2
                System.out.println("\nVENCEDOR: EXÉRCITO 2");
                batalhandoGif.setText("Você perdeu! Infelizmente não foi dessa vez... Treine suas habilidades estratégicas, meu amigo, assim poderás vencer muitas batalhas ainda!");
                break;
            case 3:
                //empate
                System.out.println("VENCEDOR: EMPATE");
                batalhandoGif.setText("Parece que as tropas estão cansadas dessa dura batalha, seus caveleiros não têm mais força para lutar e a tropa inimiga pediu trégua. Parece que não temos um vencedor nesta batalha, mas vamos nos preparar para vencer na próxima!");
        }
    }
    
    /**
     * Troca para a tela de dados da batalha
     * @param event 
     */
    @FXML
    private void dadosBatalha(ActionEvent event){
        System.out.println("Botão dados da batalha!");
        Rpg.trocaTela("Resultado.fxml");
    }

    /**
     * Inicializa e reproduz os métodos de batalhar() e exibirVencedor()
     * @param url
     * @param rb 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        b.batalhar();
        exibirVencedor();
    }
}
